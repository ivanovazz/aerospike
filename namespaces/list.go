package namespaces

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	"go-aerospike-gui/clients"
	"go-aerospike-gui/helpers"
	"go-aerospike-gui/tabs"
	"image/color"

	//"go-aerospike-gui/tabs"
	"reflect"
	//"go-aerospike-gui/clients"
)

func InitNamespaces(host string, port int, name string, myWindow fyne.Window, newTabs *container.AppTabs) *widget.Tree {
	data := clients.GetListTree(host, port, name)
	var tree *widget.Tree
	tree = widget.NewTreeWithStrings(data)
	tree.OnBranchClosed = func(id string) {
		fmt.Println(id)
	}
	tree.OnSelected = func(id string) {
		var namespace string
		for key, val := range data {
			rt := reflect.TypeOf(val)
			if (rt.Kind() == reflect.Array || rt.Kind() == reflect.Slice) && helpers.InArray(id, val) {
				namespace = key
			}

		}
		fmt.Println(namespace)

		text := canvas.NewText("", color.Black)

		ourTabs := tabs.InitTabs(host, port, namespace, id, text, newTabs)
		//ourTabs.Refresh()
		myWindow.SetContent(container.New(
			layout.NewBorderLayout(nil,text,nil,nil),
			text,
			container.New(
				layout.NewHBoxLayout(),
				InitNamespaces(host, port, name, myWindow, newTabs),
				ourTabs,
				),
			),

		)

	}

	tree.OpenAllBranches()
	return tree

}

