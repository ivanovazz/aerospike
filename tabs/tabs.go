package tabs

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/widget"
	//"fyne.io/fyne/v2/storage"
	//"fyne.io/x/fyne/widget"
	"go-aerospike-gui/clients"
	"go-aerospike-gui/tables"
	//"log"
)

func InitTabs(host string, port int, namespace string, set string, text *canvas.Text, AppTabs *container.AppTabs ) *container.AppTabs {
	scanObj := clients.Scan(host, port, namespace, set)
	fmt.Println(scanObj)
	entryKey := widget.NewEntry()
	table := tables.InitTable(scanObj, text, entryKey)
	table.Resize(fyne.Size{300000,5000})

	var tab *container.TabItem
	entryKey.Resize(fyne.NewSize(3000000000,500000000))
	entryKey.Refresh()
	btn := widget.NewButton("Search", func() {
		rec := clients.GetByKey(host, port, namespace, set, entryKey.Text)
		fmt.Println(rec)
		newTable := tables.InitTable(rec, text, entryKey)
		table.Length = newTable.Length
		table.UpdateCell = newTable.UpdateCell
		table.Resize(fyne.Size{30000,5000})
		table.Refresh()
	})
	btnClose := widget.NewButton("Close", func() {
		index := AppTabs.CurrentTabIndex()
		AppTabs.RemoveIndex(index)
	})

	//btnClose := helpers.NewTappableIcon(theme.FyneLogo())

	//icon :=  helpers.NewTappableIcon(theme.FyneLogo())
	tab = container.NewTabItem( namespace + "." + set,
	//tab = container.NewTabItemWithIcon( namespace + "." + set, helpers.NewTappableIcon(theme.FyneLogo()),
		container.New(
			layout.NewVBoxLayout(),

			container.New(layout.NewHBoxLayout(),
				container.New(layout.NewGridLayoutWithColumns(1),
					container.New(
						layout.NewVBoxLayout(), btn,
					)),
				container.New(layout.NewGridWrapLayout(fyne.NewSize(450,50)),
					container.New(
						layout.NewVBoxLayout(), entryKey,
					)),

				container.New(layout.NewGridWrapLayout(fyne.NewSize(50,37)), btnClose),
				),

			container.NewWithoutLayout(
				table,
			),

		),

	)


	AppTabs.Append(tab)
	AppTabs.SelectTab(tab)

	AppTabs.Refresh()

	return AppTabs
}

