package helpers

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/widget"
	"log"
)

type TappableIcon struct {
	widget.Icon
}

// Name returns the unique name of this resource, usually matching the file it
// was generated from.
func (r *TappableIcon) Name() string {
	return r.Icon.Resource.Name()
}

// Content returns the bytes of the bundled resource, no compression is applied
// but any compression on the resource is retained.
func (r *TappableIcon) Content() []byte {
	return r.Icon.Resource.Content()
}

func (t *TappableIcon) Tapped(_ *fyne.PointEvent) {
	log.Println("I have been tapped")
}

func (t *TappableIcon) TappedSecondary(_ *fyne.PointEvent) {
	log.Println("I have been tapped2")
}


func NewTappableIcon(res fyne.Resource) *TappableIcon {
	icon := &TappableIcon{}
	icon.ExtendBaseWidget(icon)
	icon.SetResource(res)

	return icon
}