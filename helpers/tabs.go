package helpers

import (
	"fyne.io/fyne/v2"
	"log"
)

type TappableIconTabItem struct {
	Text    string
	Icon    fyne.Resource
	Content fyne.CanvasObject
}

func (t *TappableIconTabItem) Tapped(_ *fyne.PointEvent) {
	log.Println("I have been tapped")
}

func NewTappableIconTab(text string, content fyne.CanvasObject) *TappableIconTabItem {
	return &TappableIconTabItem{Text: text, Content: content}
}