package connections

import (
	"fmt"
	"go-aerospike-gui/namespaces"
	//"encoding/json"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"strconv"
)

type connection struct {
	Connections []connectionItem
}
type connectionItem struct {
	Name string `yaml:"name"`
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
	User string `yaml:"user"`
	Pass string `yaml:"pass"`
}

func getConnections() connection {
	var c connection
	yamlFile, err := ioutil.ReadFile("./lists/data/connections.yaml")
	if err != nil {
		log.Printf("yamlFile.Get err   #%v ", err)
	}
	err = yaml.Unmarshal(yamlFile, &c)
	if err != nil {
		log.Fatalf("Unmarshal: %v", err)
	}

	return c
}
func SetConnection(conn connection)  {

	d, err := yaml.Marshal(conn)
	if err != nil {
		log.Fatalf("error: %v", err)
	}

	fmt.Println(string(d))

	err = ioutil.WriteFile("./lists/data/connections.yaml", d, 0777)
}
func ConnectionList()  {
	myApp := app.New()
	lightTheme := theme.LightTheme()
	myApp.Settings().SetTheme(lightTheme)
	myWindow := myApp.NewWindow("Aerospike Gui")
	myWindow.Resize(fyne.NewSize(400, 400))

	Accordion := getAccordion(myWindow)
	btn := widget.NewButton("Add", func() {
		modal := widget.NewModalPopUp(widget.NewLabel("modal"), myWindow.Canvas())
		form := getForm(modal, Accordion, myWindow)
		modal.Content = form
		modal.Show()
	})
	sizeBtn := btn.Size()
	sizeBtn.Width = 5
	sizeBtn.Height = 1
	btn.Resize(sizeBtn)

	//content := container.New(layout.NewGridLayout(2), Accordion, btn, layout.NewSpacer())
	//content.MinSize()
	myWindow.SetContent(
		container.New(
			layout.NewGridLayout(2),
			container.New(layout.NewVBoxLayout(), Accordion),
			container.New(layout.NewVBoxLayout(), btn, layout.NewSpacer(), layout.NewSpacer()),
			layout.NewSpacer(),
		),

	)
	myWindow.Resize(fyne.Size{800,600})
	myWindow.ShowAndRun()
}


func getAccordion(myWindow fyne.Window) *widget.Accordion {
	items := getConnections()
	var ac *widget.AccordionItem
	var Accordion *widget.Accordion
	i:=0
	Accordion = widget.NewAccordion()
	newTabs := &container.AppTabs{BaseWidget: widget.BaseWidget{}}
	for _, item := range items.Connections {
		ac = widget.NewAccordionItem(item.Name, widget.NewButton("Host: " + item.Host + ":" +
			strconv.Itoa(item.Port) + " User: " + item.User, func() {
			content := container.New(layout.NewHBoxLayout(),namespaces.InitNamespaces(item.Host, item.Port, item.Name, myWindow, newTabs))
			content.MinSize()
			myWindow.SetContent(container.New(layout.NewHBoxLayout(), content))
		}))
		i++
		Accordion.Append(ac)
	}

	return Accordion
}

func refreshAccordion(Accordion *widget.Accordion, myWindow fyne.Window) {
	items := getConnections()
	var ac *widget.AccordionItem
	i:=0

	Accordion.Items = []*widget.AccordionItem{}
	for _, val := range Accordion.Items {
		println(val.Title)
		Accordion.Remove(val)
	}
	newTabs := &container.AppTabs{BaseWidget: widget.BaseWidget{}}
	for _, item := range items.Connections {
		ac = widget.NewAccordionItem(item.Name, widget.NewButton("Host: " + item.Host + ":" +
			strconv.Itoa(item.Port) + " User: " + item.User, func() {
			content := container.New(layout.NewHBoxLayout(),namespaces.InitNamespaces(item.Host, item.Port, item.Name, myWindow,newTabs))
			content.MinSize()
			myWindow.SetContent(container.New(layout.NewHBoxLayout(), content))
		}))
		i++
		Accordion.Append(ac)
	}

	Accordion.Refresh()

}

func getForm(popup *widget.PopUp, Accordion *widget.Accordion, myWindow fyne.Window) *widget.Form  {
	entryName := widget.NewEntry()
	entryHost := widget.NewEntry()
	entryPort := widget.NewEntry()
	entryUser := widget.NewEntry()
	entryPass := widget.NewEntry()

	form := &widget.Form{
		BaseWidget: widget.BaseWidget{},
		Items: []*widget.FormItem{
			{Text: "Name", Widget: entryName},
			{Text: "Host", Widget: entryHost},
			{Text: "Port", Widget: entryPort},
			{Text: "User", Widget: entryUser},
			{Text: "Pass", Widget: entryPass},
		},
		OnSubmit: func() {
			var conn connectionItem
			conn.Name = entryName.Text
			conn.Host = entryHost.Text
			port, err := strconv.Atoi(entryPort.Text)
			PanicOnError(err)
			conn.Port = port
			conn.User = entryUser.Text
			conn.Pass = entryPass.Text
			items := getConnections()
			items.Connections = append(items.Connections, conn)
			SetConnection(items)
			refreshAccordion(Accordion, myWindow)
			log.Println("add")
			popup.Hide()
		},
		OnCancel: func() {
			popup.Hide()
			log.Println("hide")
		},
		SubmitText: "",
		CancelText: "",
	}

	return form
}
func PanicOnError(err error) {
	if err != nil {
		panic(err)
	}
}