package tables

import (
	"fmt"
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/canvas"
	"fyne.io/fyne/v2/widget"
	as "github.com/aerospike/aerospike-client-go"
	jsoniter "github.com/json-iterator/go"
	"github.com/rivo/uniseg"
	"reflect"
	"sort"
	"strings"
)

func InitTable(scanObj []as.BinMap, text *canvas.Text, entry *widget.Entry) (*widget.Table) {
	rows := len(scanObj)
	columns := 0
	println(rows)
	header := make([]string, 0)
	row := make([]string, 0)
	count := 0
	var data = make([][]string, 0)
	fmt.Println(scanObj)
	for _, val := range scanObj {
		if len(val) > columns {
			columns = len(val)
		}

		if count == 0 {
			for key, _ := range val {
				header = append(header, key)

			}
			data = append(data, header)
		}
		count++
	}
	sort.Strings(header)

		for _, valObj := range scanObj {

			for _, headVal := range header {
				for keyRow, valRow := range valObj {
					if keyRow == headVal {
						rt := reflect.TypeOf(valRow)
						if rt.Kind() == reflect.String {
							row = append(row, valRow.(string))
						} else {

							buf, err := jsoniter.Marshal(valRow)
							if err != nil {
								fmt.Println(err)
							}
							row = append(row, string(buf))
						}
					} else {
						continue
					}
				}
			}

			data = append(data, row)
			row = []string{}
		}

		fmt.Println(data)


		table := &widget.Table{
		BaseWidget: widget.BaseWidget{},
		Length: func() (int, int) {
			if len(data) > 0 {
				return len(data), len(data[0])
			} else {
				return 0,0
			}
		},
		UpdateCell: func(i widget.TableCellID, o fyne.CanvasObject) {
			widthColHead := uniseg.GraphemeClusterCount(data[0][i.Col])+4
			widthContent := uniseg.GraphemeClusterCount(data[i.Row][i.Col])
			if widthColHead > widthContent {
				o.(*widget.Label).SetText(data[i.Row][i.Col])
			} else {
				contentAllArr:= strings.Split(data[i.Row][i.Col], "")
				contentArr := contentAllArr[0:(widthColHead-1)]
				contentStr := strings.Join(contentArr,"")
				content := contentStr + "..."
				o.(*widget.Label).SetText(content)
			}

		},
		OnSelected: func(id widget.TableCellID) {

			if id.Row >= len(data) {
				return
			}

			if id.Col >= len(data[id.Row]) {
				return
			}
			if id.Col < 0  {
				return
			}

			text.Text = data[id.Row][id.Col]
			text.Refresh()
			entry.Text = data[id.Row][id.Col]
			entry.Refresh()
		},
		CreateCell: 	func() fyne.CanvasObject {
			return widget.NewLabel("new")
		},
	}
	if len(data) > 0 {
		for i := 0; i < (len(data[0])); i++ {
			widthCol := float32(uniseg.GraphemeClusterCount(data[0][i])+4)*10
			table.SetColumnWidth(i, widthCol)
		}
	}


	return table
}
