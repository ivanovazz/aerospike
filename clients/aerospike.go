package clients

import (
	"fmt"
	as "github.com/aerospike/aerospike-client-go"
	"go-aerospike-gui/helpers"
	"log"
	"strings"
	"time"
)

func initClient(host string, port int) *as.Connection {
	log.SetFlags(0)
	fmt.Println(host, port)
	// connect to the host
	cp := as.NewClientPolicy()
	cp.Timeout = 10 * time.Second
	conn, err := as.NewConnection(cp, as.NewHost(host, port))
	if err != nil {
		log.Fatalln(err.Error())
	}

	return conn


}

func GetListTree(host string, port int, headTree string)map[string][]string  {
	conn := initClient(host, port)
	namespaces := make([]string, 0)
	var tmpKey string
	var tmpVal string
	infoMapNamespaces, err := as.RequestInfo(conn, "namespaces")
	if err != nil {
		log.Fatalln(err.Error())
	}
	infoMapSets, err := as.RequestInfo(conn, "sets")
	if err != nil {
		log.Fatalln(err.Error())
	}
	var data = map[string][]string{
		"" : {headTree},
	}

	for _, v := range infoMapNamespaces {
		namespaces = append(namespaces, v)
		data[headTree] = namespaces
		data[v] = []string{}
	}

	for _, vset := range infoMapSets {
		 items := strings.Split(vset, ":")
		 tmpKey = ""
		 tmpVal = ""
		 for _, item := range items {
		 	tmpItem := strings.Split(item, "=")
		 	if (tmpItem[0] == "ns") {
				tmpKey = tmpItem[1]
			} else if (tmpItem[0] == "set") {
				tmpVal = tmpItem[1]
			}
			if helpers.InArray(tmpVal, data[tmpKey]) == false && tmpVal != "" {
				data[tmpKey] = append(data[tmpKey], tmpVal)
			}
		 }
	}

	fmt.Println(data)
	return data
}

func Scan (host string, port int, namespace string, set string) []as.BinMap {
	client, err := as.NewClient(host, port)
	if err != nil {
		log.Fatalln(err.Error())
	}

	defer client.Close()
	spolicy := as.NewScanPolicy()
	spolicy.ConcurrentNodes = true
	spolicy.Priority = as.LOW
	spolicy.IncludeBinData = true

	recs, err := client.ScanAll(spolicy, namespace, set)
	fmt.Println("namespace "+namespace)
	fmt.Println("set "+set)
	if err != nil {
		log.Fatalln(err.Error())
	}
	var result []as.BinMap
	for res := range recs.Results() {
		if res.Err != nil {
			log.Fatalln(res.Err)
		} else {
			result = append(result, res.Record.Bins)
		}
	}

	return result
}

func GetByKey(host string, port int, namespace string, set string, key string) []as.BinMap  {
	client, err := as.NewClient(host, port)
	if err != nil {
		log.Fatalln(err.Error())
	}
	aqlKey, err := as.NewKey(namespace, set, key)
	if err != nil {
		log.Fatalln(err.Error())
	}
	policy := as.NewPolicy()
	rec, err := client.Get(policy, aqlKey)
	if err != nil {
		log.Fatalln(err.Error())
	}
	var result []as.BinMap
	result = append(result, rec.Bins)

	return result
}