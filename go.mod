module go-aerospike-gui

go 1.15

require (
	fyne.io/fyne/v2 v2.0.3
	fyne.io/x/fyne v0.0.0-20210701082352-af71266c7344
	github.com/aerospike/aerospike-client-go v4.5.2+incompatible
	github.com/arangodb/go-driver v0.0.0-20210621075908-e7a6fa0cbd18 // indirect
	github.com/fyne-io/terminal v0.0.0-20210727092532-0f55e133be5e // indirect
	github.com/jain-neeeraj/OnlyAQL v0.0.0-20210307085849-1cd0a6183817 // indirect
	github.com/joho/godotenv v1.3.0 // indirect
	github.com/json-iterator/go v1.1.11
	github.com/onsi/ginkgo v1.16.4 // indirect
	github.com/onsi/gomega v1.13.0 // indirect
	github.com/rivo/uniseg v0.2.0
	github.com/yuin/gopher-lua v0.0.0-20210529063254-f4c35e4016d9 // indirect
	gopkg.in/yaml.v2 v2.4.0
)
